import Vue from 'vue'
import VueRouter from 'vue-router'

import login from '../components/login.vue'

Vue.use(VueRouter)

const routes = [
  // 登录路由
  { path: '/', redirect: '/login' }, // 重定向到login页面
  { path: '/login', component: login }
]

const router = new VueRouter({
  routes
})

export default router
